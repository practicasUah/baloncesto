Autor: Sergio Marín Barberá
Asignatura: Integración Continua en el desarrollo ágil

Partimos desde la practica guiada por los profesores ya terminada.

El profesor nos proporcionó el siguiente Servlet Java:
    - Acb.java

Para poder desarrollar la funcionalidad para borrar los votos de los jugadores 
de la Acb he tenido que usar otro servlet que es BorrarVotos.java.

Primeramente intenté integrar todo en el mismo form que nos daba hecho el profesor
en el index. Este form en el action llamaba a ACB.  Para ello utilicé otro input de tipo submit. Esto me dio problemas y errores java null exception. Capturaba bien el click y el metodo de la base de datos se ejecutaba pero me daba error.

Lo corrige con un radio button que llamé borrar botos y cuando este estaba marcado y pulsaba al input de tipo submit para borrar los botos si me funcionaba y correjia el error que comento.

A nivel de experiencia de usuario no me pareció la mejor opción asi que finalmente creé otro servlet que he llamado BorrarVotos. Para hacer uso del servlet también lo he especificado en el fichero web.xml. Simplemente con este form llamo al servlet BorrarVotos que llama al metodo de la base de datos que ejecuta una query y pone todos los registros de votos a 0.

Para ver los votos hago lo mismo creo otro form y en la action le digo que vaya a la vista JSP que he creado para mostrar los votos. Es simplemente una tabla.

Para poder visualizar los votos he tenido que crear la clase Votacion, que tiene los atributos nombre y votos. En el modelo de datos he creado un metodo que es consultar los votos. Devuelve un arrayList con todos los votos. Luego desde el jsp los recorro y los pinto en la vista.

PRUEBAS FUNCIONALES

Para codificar estas pruebas he utilizado la extensión de Selenium para el chrome. He tenido que descargarme el webdriver.chrome.driver y referenciarlo desde el fichero PruebasPhantomjsIT.java

Para ello he grabado con Selenium especificando la url del proyecto. Para la primera prueba que era borrar los votos y comprobar que estaban a 0. He hecho una votacion, luego he dado a ver los votos para comprobar que habia votos y luego he accionado el boton de Poner votos a 0. Despues al dar a ver los votos he utilizado la opcion de Selenium IDE "assert" y "text" de esta forma compruebo que el valor sea = 0 y pueda pasar el test.

Para realizar el test funcional que comprueba si al votar un nuevo jugador el voto se pone a 1 he hecho lo mismo. He votado por un jugador nuevo y luego he dado a ver los votos y he hecho un asert text para que me generase la comparación con el value = 1. Como observación decir que en este punto he borrado los votos despues de comprobar que al votar el voto era = 1. Porqué  si no la siguiente vez que corriese el test no lo pasaria porque el voto seria = 2. 


TEST UNITARIO

Entender estos test me ha costado. Primeramente intente codificarlo intentando hacer logica consumiento los metodos del modelo de datos. Es decir primero  capturaba en una variable los votos de un jugador determinado, luego llamaba al metodo que le daba un voto, y luego lo comparaba que el valor de antes fuese igual al nuevo - 1. Pero al ejecutarlo me daba un error porque no podia inicializar el modelo de datos.

Me di cuenta de que habia que utilizar Mockito. Basicamente porque con mockito podemos hacer uso de metodos que estan en clases a las que no tenemos acceso y para cierto metodo dar la salida que queramos.

Para esto me he creado 2 interfaces. 

DataService: Esta interfaz tiene el metodo que lo unico que sabemos es que devuelve los votos que tiene un jugador por ejemplo. Nos da igual como lo haga pero sabemos que nos va dar eso.

VotosService: Esta interfaz tiene un metodo que simplemente devuelve un entero y será la suma de los votos actuales + 1

Luego tenemos la clase calcularVotosImpl que impletenta la interface VotosService: desde esta clase llamamos al metodo de la clase DataService y le sumamos 1.

Desde la clase ModeloDatosTest realizamos el Test unitario con mockito:

Usamos la anotacion @Mock para la clase DataService que es la que tiene el metodo que no sabemos como esta implementado pero cuando lo llamemos nos va a devolver lo que queramos.

Para realizar el test ya sabemos que para que se cumpla cuando llamemos a sumar los votos el resultado tiene que compararse con el numero de salida que fijamos + 1 para que pase el test.
 
Observaciones:

En la aplicación en heroku no tenemos base de datos por lo que ver votos siempre saldra con 0 votos los jugadores

He modificado los stages en el fichero -gitlab-ci.yml para que cuando pase las pruebs qa me despliegue la aplicación automaticamente.

He tenido que hacer modificaciones para que pasara la pruebas de qa ya que tenia mucho major issues, por ejemplo he tenido que añadir el <!DOCTYPE html> <html lang="es"> en los ficheros html y jsp.

Tenia muchos System.out.println para depurar y he tenido que usar un logger.

Algunos mensajes como "El error es: " que utilizaba en los catch me decia que serian mejor con una constate y así lo he hecho.

Creo que esto es todo lo mas reseñable.







