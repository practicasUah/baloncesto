<!-- Los import -->
<%@ page language="java" %>
<%@ page import = "paquete.ModeloDatos"%> 
<%@ page import = "paquete.Votacion"%> 
<%@ page import = "java.util.ArrayList"%> 
<%   ModeloDatos bd = new ModeloDatos(); %>
<%   bd.abrirConexion();  %>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <title>Votación mejor jugador de la liga ACB</title>
</head>
    
    <body class="resultado">
    <h1>Votaci&oacute;n al mejor jugador de la liga ACB</h1>
        <hr>
    <br><h2>Resultados de la votaci&oacute;n</h2>
        <br>
        <table>
            <thead>
            <tr>
                <td>Nombre Jugador</td>
                <td>Numero de Votos</td>
            </tr>
            </thead>
            <tbody>
            <%
            ArrayList<Votacion> lista = bd.contultarVotos();
            bd.cerrarConexion();
            for (int i=0;i<lista.size();i++)
            {
               out.println("<tr>");
               out.println("<td>"+lista.get(i).getNombre()+"</td>");
               out.println("<td>"+lista.get(i).getNum_votos()+"</td>");
               out.println("</tr>");
            }
            %>
            </tbody>
            </table>
        <br><h3> <a href="index.html"> Ir al comienzo</a></h3>
    </body>
</html>