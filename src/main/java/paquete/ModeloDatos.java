package paquete;



import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;



public class ModeloDatos {

    final static Logger logger = Logger.getLogger(ModeloDatos.class);
    final String ERROR = "El error : ";


    private Connection con;
    private Statement set;
    private ResultSet rs;

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;

            con = DriverManager.getConnection(url, dbUser, dbPass);


        } catch (Exception e) {
            // No se ha conectado


                logger.debug(ERROR + e.getMessage());


        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla

            logger.debug("No lee de la tabla");
            logger.debug(ERROR + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {

            set = con.createStatement();

            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");

            rs.close();

            set.close();

        } catch (Exception e) {
            // No modifica la tabla
           logger.debug("No modifica la tabla");
           logger.debug(ERROR + e.getMessage());
        }
    }

    public ArrayList<Votacion> contultarVotos(){
        ArrayList<Votacion> listaVotos = new ArrayList<Votacion>();
        try {

            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                Votacion votacion = new Votacion();
                votacion.setNombre(rs.getString("Nombre"));
                votacion.setNum_votos(rs.getInt("Votos"));
                listaVotos.add(votacion);
            }
            rs.close();
            set.close();


        } catch (Exception e) {
            // No modifica la tabla
            logger.debug("No modifica la tabla");
            logger.debug(ERROR + e.getMessage());
        }
        return listaVotos;
    }

    public void eliminarVotos() {

        logger.debug("Simulación de borrado de votos prueba");
        try {

            set = con.createStatement();

            set.executeUpdate("UPDATE Jugadores SET votos=0");
            logger.debug("Termino la consulta de borrado");

            set.close();
            logger.debug("accion despues del close");


        } catch (Exception e) {

            logger.debug("No pone a 0 los votos de los jugadores en la tabla");
            logger.debug(ERROR + e.getClass().getName());
            logger.debug(e);
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.debug("No inserta en la tabla");
            logger.debug(ERROR + e.getMessage());
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

}
