package paquete;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class BorrarVotos extends HttpServlet{

    private ModeloDatos bd;
    private final String CLIENTE = "nombreCliente";

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();

    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
      bd = new ModeloDatos();
      bd.abrirConexion();
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("botonBorrar");
        
       
        

        

        if (nombre.equals("Poner votos a 0")){
            bd.eliminarVotos();
            s.setAttribute(CLIENTE, nombreP);
            res.sendRedirect(res.encodeRedirectURL("EliminarVotos.jsp"));
        }
       

        

        
    }
    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
