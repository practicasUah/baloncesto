package paquete;


import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;
    private final String CLIENTE = "nombreCliente";

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();

    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
      bd = new ModeloDatos();
      bd.abrirConexion();
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
       
        

        if (nombre.equals("Otros")) {
            nombre = (String) req.getParameter("txtOtros");
        }

        if (nombre.equals("borrarVotos")){
            bd.eliminarVotos();
            s.setAttribute(CLIENTE, nombreP);
            res.sendRedirect(res.encodeRedirectURL("EliminarVotos.jsp"));
        }else if(nombre.equals("verVotos")){
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }else if (bd.existeJugador(nombre)) {
            bd.actualizarJugador(nombre);
             s.setAttribute(CLIENTE, nombreP);
        // Llamada a la página jsp que nos da las gracias
        res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
       
        } else {
            bd.insertarJugador(nombre);

           s.setAttribute(CLIENTE, nombreP);
        // Llamada a la página jsp que nos da las gracias

        res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }
       

        

        
    }
    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
