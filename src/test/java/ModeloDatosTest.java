
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import paquete.DataService;


import static org.junit.jupiter.api.Assertions.*;


import paquete.calcularVotosImpl;

@RunWith(MockitoJUnitRunner.class)
public class ModeloDatosTest {

    @InjectMocks
    private calcularVotosImpl calcularVotos;

    @Mock
    private DataService dataService;

    @Test
    public void testActualizarVotos(){
        System.out.println("Test Actualización de votos");
        when(dataService.getListOfVotos()).thenReturn(2);
        assertEquals(3,calcularVotos.sumarVotos());
        System.out.println("Test pasado correctamente");
    }



}
