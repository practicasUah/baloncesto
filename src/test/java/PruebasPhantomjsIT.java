 
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
//

public class PruebasPhantomjsIT{
  
  
    @Test
    public void tituloIndexTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/Users/sergiomarin/Downloads/phantomjs");
        //caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),"El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

  
  


    private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @BeforeEach
  public void setUp() {
    System.setProperty("webdriver.chrome.driver","/Users/sergiomarin/Downloads/chromedriver");
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @AfterEach
  public void tearDown() {
    driver.quit();
  }




  @Test
  public void votosA0() {
    driver.get("http://localhost:8080/Baloncesto/");
    driver.manage().window().setSize(new Dimension(1280, 775));
    driver.findElement(By.name("txtNombre")).click();
    driver.findElement(By.name("txtNombre")).sendKeys("Sergio");
    driver.findElement(By.name("txtMail")).sendKeys("sergemb87@gmail.com");
    driver.findElement(By.name("R1")).click();
    driver.findElement(By.name("B1")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.name("txtNombre")).click();
    driver.findElement(By.name("txtNombre")).sendKeys("Francisco");
    driver.findElement(By.name("txtMail")).sendKeys("sergemb87@gmail.com");
    driver.findElement(By.cssSelector("p:nth-child(8) > input")).click();
    driver.findElement(By.name("B1")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.cssSelector("form:nth-child(4) > input")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.name("botonBorrar")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.cssSelector("form:nth-child(4) > input")).click();
    assertEquals(driver.findElement(By.cssSelector("tbody > tr:nth-child(1) > td:nth-child(2)")).getText(), "0");
    assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) > td:nth-child(2)")).getText(), "0");
    assertEquals(driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(2)")).getText(), "0");
  }


  @Test
  public void otroJugadorSuma1Voto() {
    driver.get("http://localhost:8080/Baloncesto/");
    driver.manage().window().setSize(new Dimension(1280, 775));
    driver.findElement(By.name("txtNombre")).click();
    driver.findElement(By.name("txtNombre")).sendKeys("Sergio");
    driver.findElement(By.name("txtMail")).sendKeys("sergemb87@gmail.com");
    driver.findElement(By.cssSelector("p:nth-child(10) > input:nth-child(1)")).click();
    driver.findElement(By.name("txtOtros")).click();
    driver.findElement(By.name("txtOtros")).sendKeys("Felipe Reyes");
    driver.findElement(By.cssSelector("p:nth-child(10)")).click();
    driver.findElement(By.name("B1")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.cssSelector("form:nth-child(4) > input")).click();
    driver.findElement(By.cssSelector("tr:nth-child(5)")).click();
    assertEquals(driver.findElement(By.cssSelector("tr:nth-child(5) > td:nth-child(2)")).getText(), "1");
    driver.findElement(By.linkText("Ir al comienzo")).click();
    driver.findElement(By.name("botonBorrar")).click();
    driver.findElement(By.linkText("Ir al comienzo")).click();
  }






    


    

    

}
